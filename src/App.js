import React from "react";
//import "./App.css";
import style from "bootstrap/dist/css/bootstrap.min.css";
import CharacterList from "./components/CharacterList/CharacterList";
import Navbar from "react-bootstrap/Navbar";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Character from "./components/Character/Character";
function App() {
  return (
    <div className="App">
      <Router>
        <Navbar bg="dark">
          <Navbar.Brand>
            <h1 style={{ color: "white" }}>Rick &amp; Morty</h1>{" "}
          </Navbar.Brand>
          <Link to="/">
            <Navbar.Brand style={{ color: "white" }}>Home</Navbar.Brand>
          </Link>
        </Navbar>
        <Switch>
          <Route path="/" exact>
            <CharacterList />
          </Route>
          <Route path="/charactercard/:id" component={Character}></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
