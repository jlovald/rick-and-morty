// Write a function component
// Simply display a character in the form of a card: https://www.w3schools.com/howto/howto_css_cards.asp
// Style the card however you like.
import Card from "react-bootstrap/Card";
import React from "react";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
// Import the CSS file as a module.
import styles from "./Character.module.css";
import Container from "react-bootstrap/Container";

class EpisodeElement extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const episodes = this.props.episodes.map(episode => {
      if (episode === undefined) return null;
      return (
        <Container className={styles.characterContainer}>
          <Card className={""}>
            <p>Name: {episode.name}</p>
            <p>Episode: {episode.episode}</p>
          </Card>
        </Container>
      );
    });
    return episodes;
  }
}

class Character extends React.Component {
  constructor(props) {
    super(props);
    let s = props.history.location.state;
    let character = s.character;
    let episodes = s.episodes;
    let filteredEpisodes = [];
    //
    //console.log(episodes);
    character.episode.forEach(e => {
      let episodeId = e.match(/[0-9].*/)[0];
      filteredEpisodes.push(episodes[parseInt(episodeId) - 1]);
    });
    //console.log(filteredEpisodes);

    this.state = {
      character: props.history.location.state.character,
      episodes: filteredEpisodes
    };
  }
  // state = {
  //     character: null
  // };
  render() {
    //console.log(this.state.episodes);
    //console.log(this.state.character);

    let character = this.state.character;
    let episodes = this.state.episodes;
    //console.log(character);
    return this.generateView(character, episodes);
  }
  generateView(character, episodes) {
    let eps = episodes;
    if (episodes === undefined) {
      eps = null;
    }
    //console.log(styles);
    return (
      <Container className={styles.character_container}>
        <Card className={styles.character_card}>
          {/* <Card.img variant="top" src={character.image}></Card.img> */}
          <img
            src={character.image}
            style={{ width: "500px" }}
            alt={character.name}
          ></img>
          <h6>Name: {character.name}</h6>
          <h6>Status: {character.status}</h6>
          <h6>Species: {character.species}</h6>
          <h6>Origin: {character.origin.name}</h6>
          <h6>Location: {character.location.name}</h6>
          <Accordion className={styles.acc}>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  Appears in:
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>
                  <EpisodeElement
                    episodes={this.generateList(episodes)}
                  ></EpisodeElement>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </Card>
      </Container>
    );
  }
  generateList(list) {
    if (list === undefined) return null;
    let elems = list.map(elem => {
      return elem;
    });

    return elems;
  }
}
export default Character;
