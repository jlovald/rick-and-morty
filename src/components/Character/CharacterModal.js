// Write a function component
// Simply display a character in the form of a card: https://www.w3schools.com/howto/howto_css_cards.asp
// Style the card however you like.
import Card from "react-bootstrap/Card";
import Modal from "react-bootstrap/Modal";
import Character from "./Character";
import React from "react";
// Import the CSS file as a module.
// import styles from "./Character.css";

class CharacterModal extends React.Component {
  constructor(props) {
    super(props);
    //console.log(props.history.location.state);
    this.state = { character: props.character, show: true };
  }

  render() {
    return (
      <>
        <Modal show={true} onHide={this.close}>
          <Modal.Header>Hi</Modal.Header>
          <Modal.Body>
            <Character character={this.state.character}></Character>
          </Modal.Body>
          <Modal.Footer> Footer</Modal.Footer>
        </Modal>
      </>
    );
  }
}
export default Character;
