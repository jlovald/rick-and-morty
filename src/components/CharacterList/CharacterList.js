import React from "react";
// Import the CSS file as a module.
import styles from "./CharacterList.module.css";
import Container from "react-bootstrap/Container";
import CharacterModal from "../Character/CharacterModal";
import Button from "react-bootstrap/Button";
import Navbar from "react-bootstrap/Navbar";
import characters from "../../DummyData.json";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Card from "react-bootstrap/Card";

const prefetched = true;

// Constant To store API url.
//const API_URL = "https://rickandmortyapi.com/api/character/";
const API_URL = "https://rickandmortyapi.com/api/character";
const EPISODE_URL = "https://rickandmortyapi.com/api/episode";
const ROUTED_URL = "https://cors-anywhere.herokuapp.com/";
const MIN_SEARCH_LENGTH = 3;

class CharacterList extends React.Component {
  // Initialize the State in Class Component.
  //  Do we want all the characters to load in one place, go by pages
  state = {
    characters: [],
    searchResults: [],
    searchStr: "",
    pages: 0,
    profileModal: null,
    episodes: [],
    pageNumber: 0
  };

  // Use ASYNC/AWAIT inside lifecycle method.
  async componentDidMount() {
    //console.("mounted");
    //console.(this.state.searchResults.length);
    if (prefetched) {
      this.setState({
        characters: characters,
        searchResults: characters
      });
      console.log(characters[0].image);
    } else {
      this.getPage(ROUTED_URL + API_URL);
      this.getGeneric(ROUTED_URL + EPISODE_URL, "episodes");
    }
  } //function getPages(from, to) {}
  async getGeneric(url, listName) {
    const response = await fetch(url).then(resp => resp.json());
    let oldList =
      this.state[listName] === undefined ? [] : this.state[listName];

    oldList.push(...response.results);
    this.setState({ listName: oldList });
    let next = response.info.next;
    if (next !== "") {
      this.getGeneric(ROUTED_URL + next, listName);
    } else {
    }
  }
  //  Low coherence, shitty code
  async getPage(url) {
    try {
      const response = await fetch(url).then(resp => resp.json());
      //console.log(url);
      let chars = [...this.state.characters];
      // Add the results from the API response.
      chars.push(...response.results);
      // ALWAYS use this.setState() in a Class Method.
      this.setState({
        characters: chars
      });
      if (this.state.searchStr.length > MIN_SEARCH_LENGTH) {
        /*let searchResults = this.state.searchResults.splice();
        
        response.results.forEach(character => {
          if (character.name.toLowerCase().includes(this.state.searchStr)) {
            searchResults.push(character);
          }
        });*/
        let searchResults = this.state.characters.filter(e => {
          e.name.toLowerCase().includes(this.state.searchStr);
        });
        this.setState({
          searchResults: searchResults
        });
      } else {
        //console.("Should print here?");
        this.setState({
          searchResults: chars
        });
      }

      let next = response.info.next;
      if (next !== "") {
        //console.(next);
        //console.log("get next");
        this.getPage(ROUTED_URL + next);
      } else {
        //console.(JSON.stringify(this.state.characters));
      }
    } catch (e) {
      console.error(e);
    }
  }
  handleSearchChange = event => {
    //console.(this);
    let query = event.target.value.toLowerCase();
    if (query.length < MIN_SEARCH_LENGTH) {
      this.setState({
        searchResults: this.state.characters,
        searchString: ""
      });
      return;
    }
    let results = [];

    this.state.characters.forEach(character => {
      if (character.name.toLowerCase().includes(query)) {
        results.push(character);
      }
    });
    //console.(results.length);
    this.setState({ searchResults: results, searchString: query });
  };

  // Required render() method in Class Component.
  render = () => {
    // Create an array of JSX to render
    let episodesRef = this.state.episodes;
    //console.("Sending! episodes");
    //console.(episodesRef);

    const searchResults = this.state.searchResults.map(character => {
      //  Alternatively a modal would be really fucking sexy.
      // to={"/charactercard/" + character.id}

      return (
        <Link
          to={{
            pathname: `/charactercard/${character.id}`,
            state: {
              character: character,
              episodes: episodesRef
            }
          }}
        >
          <Card className={styles.character_item}>
            <Card.Img variant="top" src={character.image}></Card.Img>

            <h4>{character.name}</h4>
          </Card>
        </Link>
      );
    });

    // Render MUST return valid JSX.
    // const elements = searchResults.map(element => {
    //     return <Col md={4}>{{ element }}</Col>;
    // });
    let mod = this.state.profileModal;
    return (
      <div className={styles.CharacterList}>
        <Navbar bg="light" className={styles.searchBar}>
          <input
            onChange={this.handleSearchChange}
            type="text"
            placeholder="Search for character..."
          ></input>
        </Navbar>

        <Route>
          <Container className={styles.flex_container}>
            {searchResults}
          </Container>
        </Route>
      </div>
    );
  };
}

export default CharacterList;
